import React, {Component} from 'react';
import {View, Text} from 'react-native';

import Lista from './app/components/myList';

const Validator = require('./app/components/ageValidator');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Text></Text>
        <Text></Text>
        <View>
          <Validator />
        </View>
        <Text></Text>
        <Text></Text>
        <View>
          <Lista />
        </View>
      </View>
    );
  }
}

export default App;