import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
} from 'react-native';

class myList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lista: [
        {
          nombre: 'Saint Seiya Kotz',
          img:
            'https://vignette.wikia.nocookie.net/saintseiya/images/f/f1/Saint_Seiya_Kakusei_Logo.png/revision/latest?cb=20190306175047&path-prefix=es',
          descripcion:
            'Saint Seiya Awakening: Knights of the Zodiac es un juego de rol con sistema de combate por turnos, en el que los jugadores podrán unirse a Seiya y al resto de Caballeros del Zodiaco, en una nueva aventura que les llevará a enfrentarse contra oponentes temibles. Además, podremos revivir algunos de los momentos más míticos de la serie.',
        },
        {
          nombre: 'Azur Lane',
          img:
            'https://gamehag.com/img/games/logo/azur-lane.png',
          descripcion:
            'Azur Lane es el juego de guerra naval que siempre quisiste! Cuando el poder choca con la ideología, comienza una nueva era. El mundo se está desmoronando y es tu responsabilidad crear las mejores estrategias y comandar a toda una Armada con una artillería pesada y precisa.',
        },
        {
          nombre: 'Konosuba: Fantastic Days!',
          img:
            'https://otakudesho.net/wp-content/uploads/2020/01/subarashi-presentacion_waifu2x_art_noise3_scale_tta_1.png',
          descripcion:
            'Una historia totalmente original y diseños inéditos de personajes como Kazuma, Aqua, Darkness, Wiz, Megumin, Yunyun y más. Además, Fantastic Days es un videojuego perteneciente al género de los RPG y está disponible de forma gratuita para Android y IOS.',
        },
        {
          nombre: 'Shadowverse',
          img:
            'https://shadowverse.com/en_lp/10001/img/logo.png?',
          descripcion:
            'Bienvenido a Shadowverse, un juego de cartas multijugador que combina una historia emocionante y batallas llenas de acción.Elige una de las 8 clases diferentes, cada una con un conjunto único de cartas que incluyen combatientes, hechizos y amuletos. ¡Crea tus propios mazos y descubre todas las posibilidades que guardan las más de 1000 cartas de Shadowverse!',
        },  
      ],
    };
  }
  keyExtractor = (item, index) => index.toString();

  renderItem = ({item}) => (
      <View style={styles.Contenedor}>
        <View>
          <Image
            source={{uri: item.img}}
            style={styles.Imagen}
            resizeMode="cover"
          />
        </View>
        <View style={styles.descripcion1}>
          <Text style={styles.Nombre}>{item.nombre}</Text>
          <Text style={styles.Descripcion}>{item.descripcion}</Text>
        </View>
      </View>
  );
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 35, alignSelf: 'center', fontFamily:'cursive', color:'blue',}}>
          Lista
        </Text>
        <FlatList
          data={this.state.lista}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    backgroundColor:'#E0F7FA',
  },
  Contenedor: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,

    justifyContent: 'flex-start',
    margin: 15,
  },
  Nombre: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  Descripcion: {
    fontSize: 12,
    color: 'gray',
  },
  descripcion1: {
    marginLeft: 20,
    marginRight: 80,
  },
  Imagen: {
    width: 100,
    height: 100,
    borderRadius: 25,
  },
});
export default myList;
