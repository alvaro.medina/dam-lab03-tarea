import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import { reduce } from 'traverse';

class ageValidator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Age: '',
    };
  }
  changeInput = txt => {
    console.log(txt);
    if (/^\d+$/.test(txt) || txt === '') {
      this.setState({
        Age: txt,
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        
        <Text style={styles.title}> Comprueba tu edad </Text>
        <TextInput
          style={styles.inputText}
          onChangeText={this.changeInput.bind(this)}
          keyboardType={'numeric'}
          value={this.state.Age}
        />

        <View>
          {this.state.Age == '' ? (
            <Text />
          ) : this.state.Age >= 18 ? (
            <Text style={styles.response}>Eres mayor de edad</Text>
          ) : (
            <Text style={styles.response}>Eres menor de edad</Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    padding: 20,
    backgroundColor:'#E0F7FA',
  },
  title: {
    marginTop: 15,
    alignSelf: 'center',
    fontSize: 40,
    fontFamily:"cursive",
    color: 'blue',
  },
  inputText: {
    marginTop: 25,
    borderRadius: 1,
    borderWidth: 1,
    height: 45,
  },
  response: {
    marginTop: 25,
    alignSelf: 'center',
    fontSize: 30, 
    fontFamily:"cursive",
    color:'red',
  },
});
export default ageValidator;

module.exports = ageValidator;
